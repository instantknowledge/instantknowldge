var http = require('http').createServer(handler);
var io = require('socket.io').listen(http);
var fs = require('fs');
canvas = null;
var users = {};

http.listen(4000);

function handler(req, res){
	fs.readFile(__dirname + '/convas.html', function(err,data){
		if (err){
			res.writeHead(500);
			return res.end('Error loading index.html');
		}
		res.writeHead(200);
		res.end(data);
	});
}


io.sockets.on('connection', function(socket){
	socket.on('new_user', function(data,callback){
		if( users.lenght > 0 && users.indexOf(data) != -1 )
		{
			callback(false);
		}
		else
		{
			callback(true);
			socket.user = data;
			users[socket.id] = socket.user;
			io.sockets.emit('usernames',users);
		}
	});

	console.log(socket.id + ' connected');
	socket.emit('initial_canvas',canvas);

	socket.on('client_canvas_update',function(updatedConvas){
		console.log('client update recieved on the server: '+users);
		canvas = updatedConvas;
		socket.broadcast.emit('server_canvas_update', canvas); 
	});

	// socket.on('disconnect',function(socket){
	// 	console.log(socket.id);
	// 	if(users && users[socket.id] !== null){
	// 		users.splice(socket.id,1);
	// 		socket.emit('server_new_user',users);
	// 		socket.broadcast.emit('server_new_user',users);
	// 	}
	// });

});